package main

import (
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	r := strings.NewReader("Message!")
	fmt.Printf("r length: %d\n", r.Len())

	b := make([]byte, 1)
	for {
		n, err := r.Read(b)
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Println(err)
			continue
		}
		fmt.Printf("Read %s bytes: %d!\n", b, n)
	}

	s := strings.NewReader("This is a message to write in stdout!\n")
	fmt.Printf("s length: %d\n", s.Len())
	n, err := s.WriteTo(os.Stdout)
	if err != nil {
		fmt.Println("failed to write in stdout!")
		return
	}
	fmt.Printf("wrote %d bytes to stdout!\n", n)
}
