package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func cat(filenames ...string) error {
	for _, filename := range filenames {
		f, err := os.Open(filename)
		if err != nil {
			fmt.Println(err)
			continue
		}
		defer f.Close()
		scanner := bufio.NewScanner(f)
		for scanner.Scan() {
			io.WriteString(os.Stdout, scanner.Text())
			io.WriteString(os.Stdout, "\n")
		}
	}
	return nil
}

func main() {
	arguments := os.Args
	if len(arguments) < 2 {
		fmt.Println("You have to send some file path!")
		return
	}
	cat(arguments[1:]...)
}
