package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func main() {
	s := []byte("Data to write in files, Hello world\n")

	// first method
	f1, err := os.Create("f1.txt")
	if err != nil {
		fmt.Println("Can not create file f1")
		return
	}
	defer f1.Close()
	fmt.Fprintf(f1, string(s))

	// second method
	f2, err := os.Create("f2.txt")
	if err != nil {
		fmt.Println("Can not create file, f2!")
		return
	}
	defer f2.Close()
	n, err := f2.WriteString(string(s))
	fmt.Printf("Wrote %d bytes in f2\n", n)

	// third method
	f3, err := os.Create("f3.txt")
	if err != nil {
		fmt.Println("can not create file f3")
		return
	}
	defer f3.Close()
	writer := bufio.NewWriter(f3)
	n, err = writer.WriteString(string(s))
	fmt.Printf("Wrote %d bytes of data in f3\n", n)
	writer.Flush()

	// fourth method
	f4 := "f4.txt"
	err = ioutil.WriteFile(f4, s, 0644)
	if err != nil {
		fmt.Printf("can not create to file f4\n")
		return
	}

	// fifth
	f5, err := os.Create("f5.txt")
	if err != nil {
		fmt.Println("Can not create file f5!")
	}
	defer f5.Close()
	n, err = io.WriteString(f5, string(s))
	if err != nil {
		fmt.Println("Can not write in file f5")
		return
	}
	fmt.Printf("wrote %d bytes in file f5!\n", n)
}
