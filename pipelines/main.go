package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

var closeA = false
var data = make(map[int]bool)

func random(min, max int) int {
	return rand.Intn(max-min) + min
}

func aFunction(min, max int, out chan<- int) {
	for {
		if closeA {
			close(out)
			return
		}
		out <- random(min, max)
	}
}

func bFunction(in <-chan int, out chan<- int) {
	for x := range in {
		fmt.Print(x, " ")
		_, ok := data[x]
		if ok {
			closeA = true
		} else {
			data[x] = true
			out <- x
		}
	}
	fmt.Println()
	close(out)
}

func cFunction(in <-chan int) {
	sum := 0
	for x := range in {
		sum = sum + x
	}
	fmt.Printf("the sum of random number is %d\n", sum)
}

func main() {
	args := os.Args
	if len(args) != 3 {
		fmt.Println("You have to send 2 arguments for this app")
		return
	}
	min, _ := strconv.Atoi(args[1])
	max, _ := strconv.Atoi(args[2])

	channelA := make(chan int)
	channelB := make(chan int)

	rand.Seed(time.Now().UnixNano())
	go aFunction(min, max, channelA)
	go bFunction(channelA, channelB)
	cFunction(channelB)
}
