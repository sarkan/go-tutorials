package main

import (
	"fmt"
	"io"
	"os"
	"strconv"
)

func main() {
	arguments := os.Args
	if len(arguments) != 3 {
		fmt.Println("Usage: <size> <file paht>")
		return
	}

	bufferSize, err := strconv.Atoi(arguments[1])
	if err != nil {
		fmt.Println(err)
		return
	}

	file, err := os.Open(arguments[2])
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	for {
		readData := readSize(file, bufferSize)
		if readData != nil {
			fmt.Println(string(readData))
		} else {
			break
		}
	}
}

func readSize(f *os.File, size int) []byte {
	buffer := make([]byte, size)

	n, err := f.Read(buffer)

	if err == io.EOF {
		return nil
	}

	if err != nil {
		fmt.Println(err)
		return nil
	}

	return buffer[0:n]
}
