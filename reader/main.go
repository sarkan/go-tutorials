package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"regexp"
)

func main() {
	flag.Parse()
	if len(flag.Args()) == 0 {
		fmt.Println("please add some file path to read line by line")
		return
	}
	for _, file := range flag.Args() {
		fmt.Printf("start to reader file:\t%s\n=============================\n", file)
		fmt.Println("------------------ \t Line by Line \t ------------------")
		err := readLineByLine(file)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println("------------------ \t Word by Word \t ------------------")
		err = readWordByWord(file)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println("------------------ \t Char by Char \t ------------------")
		err = readCharByChar(file)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println("========================")
	}
}

func readLineByLine(file string) error {
	var err error
	f, err := os.Open(file)
	defer f.Close()

	if err != nil {
		fmt.Println("this file is not exists")
		return err
	}
	r := bufio.NewReader(f)
	for {
		line, err := r.ReadString('\n')
		if err == io.EOF {
			break
		} else if err != nil {
			fmt.Println("Some error happend")
			return err
		}
		fmt.Printf(line)
	}
	return nil
}

func readWordByWord(file string) error {
	var err error
	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()

	r := bufio.NewReader(f)
	for {
		line, err := r.ReadString('\n')
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}
		reg := regexp.MustCompile("[^\\s]+")
		words := reg.FindAllString(line, -1)
		for i := 0; i < len(words); i++ {
			fmt.Println(words[i])
		}
	}
	return nil
}

func readCharByChar(file string) error {
	var err error
	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()

	r := bufio.NewReader(f)
	for {
		line, err := r.ReadString('\n')
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}
		for _, x := range line {
			fmt.Printf(" %s ", string(x))
		}
		fmt.Println()
	}
	return nil

}
