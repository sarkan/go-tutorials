package main

import (
	"encoding/csv"
	"fmt"
	"os"
	"strconv"

	"github.com/Arafatk/glot"
)

func main() {
	arguments := os.Args
	if len(arguments) != 2 {
		fmt.Println("This app need a data path!")
		return
	}

	_, err := os.Stat(arguments[1])
	if err != nil {
		fmt.Println("can not stat this file!")
		return
	}

	f, err := os.Open(arguments[1])
	if err != nil {
		fmt.Println("can not open file")
		fmt.Println(err)
		return
	}

	reader := csv.NewReader(f)
	reader.FieldsPerRecord = -1
	allRecord, err := reader.ReadAll()
	if err != nil {
		fmt.Println(err)
		return
	}
	xp := []float64{}
	yp := []float64{}
	for _, rec := range allRecord {
		x, _ := strconv.ParseFloat(rec[0], 64)
		y, _ := strconv.ParseFloat(rec[1], 64)
		xp = append(xp, x)
		yp = append(yp, y)
	}

	points := [][]float64{}
	points = append(points, xp)
	points = append(points, yp)
	fmt.Println(points)

	// glot for present
	dimensions := 2
	persist := true
	debug := false
	plot, _ := glot.NewPlot(dimensions, persist, debug)

	plot.SetTitle("Using Glot with csv data")
	plot.SetXLabel("X Axis")
	plot.SetYLabel("Y Axis")
	style := "circle"
	plot.AddPointGroup("Sample Data:", style, points)
	plot.SavePlot("output.png")
}
