package main

import (
	"flag"
	"fmt"
	"sync"
)

func main() {
	n := flag.Int("n", 10, "go routines count")
	flag.Parse()
	count := *n
	fmt.Printf("Going to create %d go routines\n", count)
	var waitGroup sync.WaitGroup

	fmt.Printf("%v\n", waitGroup)
	for i := 0; i < count; i++ {
		waitGroup.Add(1)
		go func(x int) {
			defer waitGroup.Done()
			fmt.Printf("%d go routine\n", x)
		}(i)
	}

	fmt.Printf("waitGroup: \n%v\n", waitGroup)
	waitGroup.Wait()
	fmt.Printf("going to exit...\n")
}
