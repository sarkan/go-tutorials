package main

import (
	"fmt"
	"os"
)

func main() {
	arguments := os.Args
	if len(arguments) < 2 {
		fmt.Println("This program need a file path to get its permissions!")
		return
	}
	getPermissions(arguments[1:]...)
}

func getPermissions(filenames ...string) {
	for _, filename := range filenames {
		info, _ := os.Stat(filename)
		mode := info.Mode()

		fmt.Printf("filename: %s\tPermissions: %s\n", filename, mode.String()[1:10])
	}
}