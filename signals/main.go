package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func signalHandle(signal os.Signal) {
	fmt.Println("signalHandle() Coutgh: ", signal)
}

func main() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt, syscall.SIGKILL)
	// signal.Notify(sigs) to get all signals
	go func() {
		for {
			sig := <-sigs
			switch sig {
			case os.Interrupt:
				fmt.Println("coutgh: ", sig)
			case syscall.SIGKILL:
				signalHandle(sig)
				return
			}
		}
	}()
	for {
		fmt.Printf(".")
		time.Sleep(5 * time.Second)
	}
}
